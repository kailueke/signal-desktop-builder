# RingRTC Builder

## Usage
1. Run setup.sh. This will prompt for root privileges to install build dependancies.
2. Source .cargo/env to set up rust
3. Run ringrtc-builder.sh.
4. Copy the output file `libringrtc-arm64.node` into the main `signal-desktop-builder` directory.
